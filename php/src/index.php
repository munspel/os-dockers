<?php 
$mysqli = mysqli_connect("db","root","password","todo_db");

// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
}
$sql = "SELECT * from todo";
if ($result = $mysqli->query($sql)) {
	echo "<ul>\n";
	while ($row = $result->fetch_assoc()) {
		echo "<li>". $row['name'] . "</li>";
	}
	echo "</ul>";
} else {
	echo "No rows!";
}
?>
