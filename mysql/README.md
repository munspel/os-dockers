# How to use

```sh
docker system prune 

docker build -t munspel/mysql .

docker run --name mysql --rm -d -v D:\dockers\mysql\data:/var/lib/mysql -p 3307:3306 munspel/mysql

docker logs mysql

docker exec -it mysql /bin/bash

root@1d6507500580:/# printenv

docker run --name myadmin -d --link mysql:db -p 8085:80 phpmyadmin/phpmyadmin
```